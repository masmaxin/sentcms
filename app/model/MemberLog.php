<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\model;

use think\Model;
use xin\helper\Server;

/**
 * @title: 用户日志模型
 */
class MemberLog extends Model {

	protected $type = [
		'param'       => 'json',
		'visite_time' => 'timestamp',
	];

	public static function record($request) {
		$data = [
			'uid'         => $request->user['uid'],
			'url'         => $request->baseUrl(),
			'param'       => $request->param(),
			'method'      => $request->method(),
			'visite_time' => $request->time(),
			'client_ip'   => Server::getRemoteIp(),
			'create_time' => time(),
		];
		self::create($data);
	}

	public function getMemberLogList($request) {
		$param = $request->param();
		$map   = [];
		$order = "id desc";

		return self::with(['user'])->where($map)->order($order)->paginate($request->pageConfig);
	}

	public function user() {
		return $this->hasOne('Member', 'uid', 'uid')->field('uid,nickname,username');
	}
}
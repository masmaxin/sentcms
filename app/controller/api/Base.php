<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\controller\api;

class Base {

	public $middleware = [
		'\app\http\middleware\Validate',
		// 'sent\jwt\middleware\JWTAuth' => ['except' => ['login']],
		'\app\http\middleware\ApiAuth',
		'\app\http\middleware\Api',
		// '\app\http\middleware\AllowCrossDomain',
	];

	protected $data = ['data' => [], 'code' => 0, 'msg' => ''];

	protected function initialize() {
	}

	protected function success($msg, $url = '') {
		$this->data['code'] = 1;
		$this->data['msg']  = $msg;
		$this->data['url']  = $url ? $url->__toString() : '';
		return $this->data;
	}

	protected function error($msg, $url = '') {
		$this->data['code'] = 0;
		$this->data['msg']  = $msg;
		$this->data['url']  = $url ? $url->__toString() : '';
		return $this->data;
	}
}
<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
namespace app\http\middleware;

use think\facade\Session;

/**
 * @title 后台中间件
 */
class Admin {

	public function handle($request, \Closure $next) {
		$request->rootUid = env('rootuid');
		$request->user = Session::get('userInfo');
		$request->url = str_replace(".", "/", strtolower($request->controller())) . '/' . $request->action();

		$request->pageConfig = array(
			'list_rows' => $request->param('limit', 20),
			'page' => $request->param('page', 1),
			'query' => $request->param()
		);

		$response = $next($request);
		return $response;
	}
}